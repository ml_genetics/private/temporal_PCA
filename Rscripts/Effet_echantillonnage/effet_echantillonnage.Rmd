---
  title: "Effet de l'échantillonage temporel sur l'ACP"
  output:
    html_notebook:
      code_folding: hide
      toc: true
      toc_float: true
---
A chaque fois, le même scénario mais des nombres d'échantillons différents.

# Scénario 1
Une population, échantillonnage toutes les 1000 générations entre 0 et 35000 générations.

## 1 moderne, 1/ancien
2700 SNPs

```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling3.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 10 modernes, 1/ancien
3085 SNPs

```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling1.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 100 modernes, 1/ancien
3566 SNPs

```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling4.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 1000 modernes, 1/ancien
3566 SNPs

```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling5.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

12870 SNPs
```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling5bis.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 10 modernes, 10/ancien
3336 SNPs

```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling2.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 1000 modernes, 10/ancien
3042 SNPs

```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling6.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", title="  ", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0),
       bty="n", ncol=2)
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## Conclusions
- Dans tous les cas, on a observé un patron en "fer à cheval".
- Quand le nombre d'échantillons modernes est égal ou environ égal au nombre d'échantillons anciens par sampling time, la PC1 est linéaire et la PC2 est une parabole (sur les données centrées et réduites).
- Quand le nombre d'échantillons modernes est très supérieur au nombre d'échantillons anciens, PC1 et PC2 sont déformées. PC1 devient courbe.


# Scénario 2
Une population, admixture il y a 500 générations. Echantillonnage avant et après le métissage.

## 1 moderne, 1/ancien
17927 SNPs
```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling2-3.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
# data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0))
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```
Ici, les données sont seulement centrées, pas réduites.

## 10 modernes, 1/ancien
17424 SNPs
```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling2-1.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0))
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 100 modernes, 1/ancien
18094 SNPs
```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling2-4.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0))
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 1000 modernes, 1/ancien
17528 SNPs
```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling2-5.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0))
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```

## 10 modernes, 10/ancien
18219 SNPs
```{r}
genotype = read.table("~/Documents/jupyter_notebooks/sampling2-2.txt", sep="\t", header=F)
data = genotype[,-c(1)]
ages = genotype[,1]

data = as.matrix(data)
dd = dim(data)
N = dd[1]
M = dd[2]
rownames(data) = ages

data = data - matrix( rep( apply(data,MARGIN=2,mean), N), nrow = N, byrow = T  )
data = data / matrix( rep( apply(data,MARGIN=2,sd), N), nrow = N, byrow = T  )

fac = factor(rownames(data))
colors = rainbow(length(unique(fac)))

pc = prcomp(data)

m = par()$mar
par(mar=m+c(0,0,0,8), xpd=TRUE)
plot(pc$x, col=colors[fac], pch=19)
legend("topright", pch=19, legend=unique(fac), col=colors[unique(fac)], cex=0.7, inset=c(-0.3,0))
par(mar=m)

par(mfrow=c(1,2))
for(i in 1:2) plot(ages, pc$x[,i], xlab="Age (generations)", ylab=paste("PC", i, sep=""), col=colors[fac], pch=19)
par(mfrow=c(1,1))
```
