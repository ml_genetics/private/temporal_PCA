## ----setup, include = FALSE----------------------------------------------
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)

## ----echo=FALSE----------------------------------------------------------
library(temporalDR)
data("onepop")
Y <- onepop[, -1]
ages <- onepop[, 1]

Y <- as.matrix(Y)
rownames(Y) <- ages

jColors <- with(onepop, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))

pc <- prcomp(Y)

par(mar=c(5.1, 4.1, 4.1, 8.1), xpd = TRUE)
plot(pc$x, pch = 19, col = jColors$color[match(rownames(pc$x), jColors$ages)],
      main = "Temporal PCA")
legend("topright", legend = as.character(jColors$ages), 
       col = jColors$color, pch=19, cex=.6, title = "Ages (in generations)",
       ncol = 3, inset = c(-0.3,0))



## ------------------------------------------------------------------------
library(temporalDR)

## ------------------------------------------------------------------------
## Simulated genotypes for time differentiation in one population
data("onepop")

## ------------------------------------------------------------------------
## Complete data set of simulated genotypes
data("admixture_scenario")

## Incomplete data set of simulated genotypes
data("admixture_scenario_NA")

## ------------------------------------------------------------------------
data("onepop")
sum(is.na(onepop))

## ------------------------------------------------------------------------
colnames(onepop)[1]
onepop$ages

## ------------------------------------------------------------------------
# A vector of ages
ages <- onepop$ages

# The genotypes
Y <- onepop[, -1]

## ------------------------------------------------------------------------
scales <- seq(9e-9, 4e-8, length = 20)
errs_e <- errors_tFA(Y = Y, 
                     ages = ages, 
                     naHandling = "mean", 
                     K = 3, 
                     scales = scales, 
                     covMat = "exp", 
                     showPlot = TRUE,
                     showProgress = FALSE,
                     seed = 10)

## ------------------------------------------------------------------------
s <- errs_e$scales[which(errs_e$errors == min(errs_e$errors))]
set.seed(10)
obj_e <- temporal_DR(Y = Y, 
                     ages = ages, 
                     K = 3, 
                     scale = s, 
                     covMat = "exp", 
                     plotPCA = TRUE, 
                     plotFA = TRUE)
# We could also use the 'tFA' function:
# obj <- tFA(Y, ages, K = 3, scale = s, covMat = "exp", plotPCA = TRUE, plotFA = TRUE)

## ------------------------------------------------------------------------
K <- seq(5, 40)
errs_b <- errors_tFA(Y = Y, 
                     ages = ages, 
                     naHandling = "mean", 
                     K = K,
                     covMat = "brownian", 
                     showPlot = TRUE,
                     showProgress = FALSE,
                     seed = 10)

## ------------------------------------------------------------------------
obj_b <- temporal_DR(Y = Y, 
                     ages = ages, 
                     K = 17, 
                     correctionMethod = "tFA",
                     covMat = "brownian",
                     plotPCA = FALSE, 
                     plotFA = TRUE)

## ------------------------------------------------------------------------
data("admixture_scenario_NA")
sum(is.na(admixture_scenario_NA))

## ------------------------------------------------------------------------
# PCA of the original complete data set:
data("admixture_scenario")
Yc <- as.matrix(admixture_scenario[, -1])
rownames(Yc) <- admixture_scenario$ages

jColors <- with(admixture_scenario, data.frame(ages = levels(as.factor(ages)),
                                    color = I(rainbow(length(unique(ages))))))

pc <- prcomp(Yc)

par(mar=c(5.1, 4.1, 4.1, 8.1), xpd = TRUE)
plot(pc$x, col = jColors$color[match(rownames(pc$x), jColors$ages)],
     pch = 19, main = "Original data set")
legend("right", legend = as.character(jColors$ages), 
       col = jColors$color, pch=19, cex=.8, title = "Ages (in generations)",
       ncol = 3, inset = c(-0.32,0))

