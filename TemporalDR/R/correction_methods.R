tFA <- function(Y, ages, K = 3, scale = 1e-6, covMat = c("exp", "brownian"), plotPCA = TRUE, plotFA = TRUE, pch = 19){
  #' @title Temporal Factor Analysis
  #' @description A factor analysis model which introduces temporal information in an explicit way to correct for temporal
  #' autocorrelation in genotypic data sets.
  #'
  #' Run \code{RShowDoc("temporalDR", package = "temporalDR")} to see the vignette for more information.
  #'
  #' @param Y A data frame or matrix of size N samples x M loci.
  #' @param ages A numeric vector of length N containing the ages of the samples.
  #' @param K Integer, the number of latent factors.
  #' @param scale Float, parameter to tune in the exponential covariance matrix.
  #' @param covMat The covariance matrix. A character string between "exp" and "brownian". See
  #' the Details section for more information.
  #' @param plotPCA Boolean. If \code{TRUE} standard PCA is performed (on centered and
  #' scaled data) and plotted.
  #' @param plotFA Boolean. If \code{TRUE}, plots the corrected graph.
  #' @param pch Integer or vector of integers. Specify symbols to use when plotting points.
  #'
  #' @details
  #' \strong{Model:}
  #'
  #' "tFA" is a matrix factorization similar to spFA (\href{https://www.frontiersin.org/articles/10.3389/fgene.2012.00254/full}{Frichot et al., 2012}):
  #' \deqn{Y = UV^T + E}
  #' with \eqn{U} the scores matrix, \eqn{V^T} the transpose of the loadings matrix and \eqn{E}
  #' the matrix of unknown noise variables. \code{tFA} returns U and V.
  #'
  #' \strong{Covariance matrix:}
  #'
  #' \itemize{
  #'     \item If \code{covMat = "exp"}, \code{tFA} uses a covariance matrix \code{Sigma = exp(-d(A_i, A_j)/theta)} with
  #'         \code{d(A_i, A_j)} the euclidean temporal distance between sample of age \code{A_i} and sample
  #'         of age \code{A_j}. The parameter \code{theta} is defined as \code{theta = scale * mean(d(A_i, A_j))}
  #'         with \code{scale} a parameter to tune.
  #'     \item If \code{covMat = "brownian"}, \code{tFA} uses a Brownian covariance matrix \code{Sigma = min(s,t)}
  #'         with \code{s = Tmax - A_i, t = Tmax - A_j} and \code{Tmax = max(ages)}.
  #' }
  #' In either case, a small random perturbation is added to ages to avoid colinearity when computing the covariance matrix.
  #' Sometimes, the perturbation does not give a positive definite covariance matrix. If it is not positive definite, the
  #' tFA model cannot be solved. The \code{tFA} function tries several values of \code{\link[base]{.Random.seed}} if necessary,
  #' and return the .Random.seed for which a positive definite covariance matrix as been computed. Using this seed allows
  #' to reproduce results.
  #'
  #' The exponential covariance matrix uses a centered data set to solve the model, while the brownian covariance
  #' uses a 'raw' data set (neither centered nor scaled).
  #'
  #' Exponential and Brownian errors ('obj$covFitError' and 'obj$error') are not comparable because of the
  #' different order of magnitude of the custom covariance matrices.
  #'
  #' For more details, run \code{RShowDoc("temporalDR", package = "temporalDR")}.
  #'
  #' @return A list of components
  #'     \item{covMat}{The type of covariance matrix: "exp" or "brownian".}
  #'     \item{U}{The scores matrix, of size N x K.}
  #'     \item{V}{The loadings matrix, of size M x K.}
  #'     \item{Sigma}{The covariance matrix, of size N x N.}
  #'     \item{E}{The residual matrix, of size N x M.}
  #'     \item{error}{The mean squared error of matrix E. This error will be high with \code{covMat == "brownian"}
  #'     since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}
  #'     \item{theta}{Tuned parameter in the exponential \code{Sigma}. If \code{covMat = "brownian"},
  #'     this parameter is NULL.}
  #'     \item{realCov}{The real sample covariance matrix, of size N x N. Computed on the raw \code{Y}
  #'     if \code{covMat = "brownian"} or on the centered and scaled data set if \code{covMat = "exp"}.}
  #'     \item{covFitError}{The mean squared of the difference \eqn{realCov - Sigma}. This error will be high with \code{covMat == "brownian"}
  #'     since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}
  #'     \item{randomSeed}{Vector. The `.Random.seed` for which a positive definite covariance matrix as been computed.}
  #'
  #'     If \code{plotFA} is \code{TRUE}, the first two columns of \code{U} (equivalent to principal components) gotten from
  #'     the correction method are plotted. \cr
  #'     If \code{plotPCA} is \code{TRUE}, standard PCA is also plotted, for comparison.
  #'
  #' @examples
  #' ## Load the data:
  #' data("onepop")
  #' Y <- onepop[, -1]
  #' ages <- onepop$ages
  #'
  #' ## tFA with exponential covariance matrix
  #' obj1 <- tFA(Y = Y,
  #'             ages = ages,
  #'             K = 3,
  #'             scale = 2e-08,
  #'             covMat = "exp")
  #'
  #' ## tFA with Brownian covariance matrix
  #' obj2 <- tFA(Y = Y,
  #'            ages = ages,
  #'            K = 15,
  #'            covMat = "brownian")
  #'
  #'
  #' @seealso
  #' \code{\link{temporal_DR}}, \code{\link{imputeNA}}, \code{\link{tRCA}}, \code{\link{tLFMM}}, \code{\link{errors_tFA}}
  #'
  #' @references
  #' Frichot E,  Schoville SD,  Bouchard G,  François O. Correcting principal component maps
  #' for effects of spatial autocorrelation in population genetic data, \emph{Front Genet.} , 2012,
  #' vol. 3 pg. 254
  #'
  #' @export

  covMat <- match.arg(covMat)
  if (K < 2) stop("Argument 'K' must be >= 2.")
  if (!identical(length(ages), nrow(Y)))
    stop("Length of 'ages' should be equal to the number of rows of 'Y'.")

  Y <- as.matrix(Y)
  if (length(ages) >= 30) ncol = 3
  else ncol = 2

  ## Center and scale the data -------
  data <- center_and_scale(Y, ages)

  ## Plots' colors -------------------
  dat <- data.frame(ages = ages, Y)
  jColors <- with(dat, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))

  ## Standard PCA --------------------
  if (plotPCA){
    standard_PCA(dat, jColors, pch, K)
    if (!plotFA){
      plot.new()
      legend("center", legend = as.character(jColors$ages),
             col = jColors$color, pch = pch, title = "Ages",
             ncol = ncol)
    }
  }

  initialSeed <- .Random.seed
  maxit <- 1000
  it <- 0
  ## Compute Sigma, the covariance matrix -------
  posDef <- FALSE
  while (!posDef && it < maxit){
    randomSeed <- .Random.seed
    it <- it + 1
    if (covMat == "exp"){
      ## Real covariance matrix
      realCov <- stats::cov(t(data))
      ## Exponential covariance matrix
      res <- expo_covar(ages, scale, perturbation = TRUE)
      theta <- res$theta
      Sigma <- res$Sigma
      posDef <- matrixcalc::is.positive.definite(Sigma)
      if (posDef){
        ## Solve the model -----------------
        Ch <- chol(solve(Sigma))
        res <- svd(Ch %*% data, K, K)
      }
      if (it == maxit) stop("Covariance matrix Sigma is not positive definite. Please try again with an other value of parameter 'scale'.")
    } else if (covMat == "brownian"){
      ## Real covariance matrix
      realCov <- stats::cov(t(Y))
      ## Brownian covariance matrix
      Sigma <- brownian_covar(ages, perturbation = TRUE)
      theta <- NULL
      posDef <- matrixcalc::is.positive.definite(Sigma)
      if (posDef){
        ## Solve the model -----------------
        Ch <- try(chol(solve(Sigma)), silent=TRUE)
        if (class(Ch) == "try-error") posDef <- FALSE
        if (posDef) res <- svd(Ch %*% Y, K, K)
      }
      if (it == maxit) stop("The covariance matrix Sigma is not positive definite.")
    }
  }

  res2 <- svd(solve(Ch) %*% res$u %*% diag(res$d[seq_len(K)]) %*% t(res$v), K, K)

  U <- res2$u %*% diag(res2$d[seq_len(K)])
  V <- res2$v

  rownames(U) <- ages

  ## Compute the error --------------------
  epsilon <- MASS::mvrnorm(dim(Y)[2], rep(0, dim(Y)[1]), Sigma)
  E <- Y - U %*% t(V) - t(epsilon)
  err <- mse(E)

  ## Compute the covariance matrices fit error ------
  C <- realCov - Sigma
  covFitError <- mse(C)

  l <- list("covMat" = covMat,
            "U" = U,
            "V" = V,
            "Sigma" = Sigma,
            "E" = E,
            "theta" = theta,
            "error" = err,
            "realCov" = realCov,
            "covFitError" = covFitError,
            "randomSeed" = randomSeed)

  if (covMat == "exp" && plotFA){
    plot_FA(U, ages, jColors, pch,
            title = paste("tFA - Exponential covariance matrix\ntheta = ",
                          signif(theta, digits = 4),
                          "\tscale = ",
                          signif(scale, digits = 4),
                          "\tK = ", K, sep = ""))
  } else if (covMat == "brownian" && plotFA){
    plot_FA(U, ages, jColors, pch,
            title = paste("tFA - Brownian covariance matrix\nK = ", K, sep = ""))
  }

  if (plotFA){
    plot.new()
    legend("center", legend = as.character(jColors$ages),
           col = jColors$color, pch = pch, title = "Ages",
           ncol = ncol)
  }

  .Random.seed <- initialSeed
  return(l)
}


tLFMM <- function(Y, ages, q = 5, K = 3, lambda = 1, scale = 1e-6, covMat = c("exp", "brownian"), plotPCA = TRUE, plotFA = TRUE, pch = 19){
  #' @title Temporal LFMM
  #' @description This function corrects for temporal autocorrelation in genotypic data sets
  #' by computing regularized least squares estimates for latent factor mixed models using
  #' a ridge penalty.
  #'
  #' Run \code{RShowDoc("temporalDR", package = "temporalDR")} to see the vignette for more information.
  #'
  #' @param Y A data frame or matrix of size N samples x M loci.
  #' @param ages A numeric vector of length N containing the ages of the samples.
  #' @param q Integer, the number of selected eigenvectors of the Brownian covariance matrix.
  #' @param K Integer, the number of latent factors.
  #' @param lambda The ridge regularization parameter.
  #' @param scale Float, parameter to tune in the exponential covariance matrix.
  #' @param covMat The covariance matrix. A character string between "exp" and "brownian". See
  #' the Details section for more information.
  #' @param plotPCA Boolean. If \code{TRUE} standard PCA is performed (on centered and
  #' scaled data) and plotted.
  #' @param plotFA Boolean. If \code{TRUE}, plots the corrected graph.
  #' @param pch Integer or vector of integers. Specify symbols to use when plotting points.
  #'
  #' @details
  #' \strong{Model:}
  #'
  #' "tLFMM" uses the \code{lfmm::lfmm_ridge()} function (\href{https://www.biorxiv.org/content/biorxiv/early/2018/05/13/255893.full.pdf}{Caye and François, 2018})
  #' with the latent factor mixed model:
  #' \deqn{Y = UV^T + ZW^T +E}
  #' where \eqn{U} and \eqn{V} are respectively the latent effects scores and loadings matrices, \eqn{Z} and \eqn{W} are respectively
  #' the fixed effects scores and loadings matrices, and \eqn{E} id the matrix of unknown noise variables. \eqn{Z} is given to \code{tLFMM} which returns
  #' \eqn{U}, \eqn{V} and \eqn{W}.
  #'
  #' \strong{Covariance matrix:}
  #'
  #' \itemize{
  #'     \item If \code{covMat = "exp"}, \code{tLFMM} uses a covariance matrix \code{Sigma = exp(-d(A_i, A_j)/theta)} with
  #'         \code{d(A_i, A_j)} the euclidean temporal distance between sample of age \code{A_i} and sample
  #'         of age \code{A_j}. The parameter \code{theta} is defined as \code{theta = scale * mean(d(A_i, A_j))}
  #'         with \code{scale} a parameter to tune.
  #'     \item If \code{covMat = "brownian"}, \code{tLFMM} uses a Brownian covariance matrix \code{Sigma = min(s,t)}
  #'         with \code{s = Tmax - A_i, t = Tmax - A_j} and \code{Tmax = max(ages)}.
  #' }
  #'
  #' The exponential covariance matrix uses a centered data set to solve the model, while the brownian covariance
  #' uses a 'raw' data set (neither centered nor scaled).
  #'
  #' Exponential and Brownian errors ('obj$covFitError' and 'obj$error') are not comparable because of the
  #' different order of magnitude of the custom covariance matrices.
  #'
  #' For more details, run \code{RShowDoc("temporalDR", package = "temporalDR")}.
  #'
  #' @return A list of components
  #'      \item{U}{The latent effects scores matrix, of size N x K.}
  #'      \item{V}{The latent effects loadings matrix, of size M x K.}
  #'      \item{Z}{The fixed effects scores matrix, of size N x q.}
  #'      \item{W}{The fixed effects loadings matrix, of size M x q.}
  #'      \item{E}{The residual matrix, of size N x M.}
  #'      \item{Sigma}{The covariance matrix, of size N x N.}
  #'      \item{error}{The mean squared error of matrix E. This error will be high with \code{covMat == "brownian"}
  #'     since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}
  #'      \item{theta}{Tuned parameter in the exponential \code{Sigma}. If \code{covMat = "brownian"},
  #'      this parameter is NULL.}
  #'      \item{realCov}{The real sample covariance matrix, of size N x N. Computed on the raw \code{Y}
  #'      if \code{covMat = "brownian"} or on the centered and scaled data set if \code{covMat = "exp"}.}
  #'      \item{covFitError}{The mean squared of the difference \eqn{realCov - (UU^T + Sigma)}. This error will be high with \code{covMat == "brownian"}
  #'     since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}
  #'
  #'      If \code{plotFA} is \code{TRUE}, the first two columns of \code{U} (equivalent to principal components) gotten from
  #'     the correction method are plotted. \cr
  #'      If \code{plotPCA} is \code{TRUE}, standard PCA is also plotted, for comparison.
  #'
  #' @examples
  #' ## Load the data:
  #' data("onepop")
  #' Y <- onepop[, -1]
  #' ages <- onepop$ages
  #'
  #' ## tLFMM with exponential covariance matrix
  #' obj1 <- tLFMM(Y= Y,
  #'               ages = ages,
  #'               K = 3,
  #'               lambda = 1e-5,
  #'               scale = 1.5e-8,
  #'               covMat = "exp")
  #'
  #' ## tLFMM with Brownian covariance matrix
  #' obj2 <- tLFMM(Y = Y,
  #'               ages = ages,
  #'               q = 5,
  #'               K = 3,
  #'               lambda = 1e-5,
  #'               covMat = "brownian")
  #'
  #' @references
  #' Caye K, Francois O. LFMM 2.0: Latent factor models for confounder
  #' adjustment in genome and epigenome-wide association studies,
  #' \emph{Biorxiv}, 2018, \url{https://doi.org/10.1101/255893}
  #'
  #' @seealso
  #' \code{\link{temporal_DR}}, \code{\link{imputeNA}}, \code{\link{tFA}}, \code{\link{tRCA}}, \code{\link{errors_tLFMM}}
  #'
  #' @export

  covMat <- match.arg(covMat)
  if (K < 2) stop("Argument 'K' must be >= 2.")
  if (!identical(length(ages), nrow(Y)))
    stop("Length of 'ages' should be equal to the number of rows of 'Y'.")

  Y <- as.matrix(Y)
  if (length(ages) >= 30) ncol = 3
  else ncol = 2

  ## Center and scale the data -------
  data <- center_and_scale(Y, ages)

  ## Plots' colors -------------------
  dat <- data.frame(ages = ages, Y)
  jColors <- with(dat, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))

  ## Standard PCA --------------------
  if (plotPCA){
    standard_PCA(dat, jColors, pch, K)
    if (!plotFA){
      plot.new()
      legend("center", legend = as.character(jColors$ages),
             col = jColors$color, pch = pch, title = "Ages",
             ncol = ncol)
    }
  }

  ## Compute Sigma, the covariance matrix -------
  if (covMat == "exp"){
    ## Real covariance matrix
    realCov <- stats::cov(t(data))
    ## Exponential covariance matrix
    res <- expo_covar(ages, scale, perturbation = FALSE)
    theta <- res$theta
    Sigma <- res$Sigma
    z <- eigen(Sigma)
    zq <- z$vectors[, seq_len(q)]
    ## Solve the model ---------------------------------------
    mod <- lfmm::lfmm_ridge(data, zq, K = K, lambda = lambda)
  } else if (covMat == "brownian"){
    ## Real covariance matrix
    realCov <- stats::cov(t(Y))
    ## Brownian covariance matrix
    theta <- NULL
    Sigma <- brownian_covar(ages, perturbation = FALSE)
    ## Spectral decomposition of the covariance matrix -------
    z <- eigen(Sigma)
    zq <- z$vectors[, seq_len(q)]
    ## Solve the model ---------------------------------------
    mod <- lfmm::lfmm_ridge(Y, zq, K = K, lambda = lambda)
  }

  U <- mod$U
  V <- mod$V
  W <- mod$B

  rownames(U) <- ages

  ## Compute the error --------------------
  E <- Y - (U %*% t(V) + zq %*% t(W))
  err <- mse(E)

  ## Compute the covariance matrices fit error ------
  C <- realCov - (U %*% t(U) + Sigma)
  covFitError <- mse(C)

  l <- list("U" = U,
            "V" = V,
            "Z" = zq,
            "W" = W,
            "E" = E,
            "Sigma" = Sigma,
            "theta" = theta,
            "error" = err,
            "covMat" = covMat,
            "realCov" = realCov,
            "covFitError" = covFitError)

  if (covMat == "exp" && plotFA){
    plot_FA(U, ages, jColors, pch,
            title = paste("tLFMM - Exponential covariance matrix\ntheta = ",
                          signif(theta, digits = 4),
                          "\tscale = ", signif(scale, digits = 4),
                          "\tK = ", K,
                          "\tq = ", q,
                          "\tlambda = ", lambda,
                          sep = ""))
  } else if (covMat == "brownian" && plotFA){
    plot_FA(U, ages, jColors, pch,
            title = paste("tLFMM - Brownian covariance matrix\nK = ", K, "\tq = ", q,
                          "\tlambda = ", lambda, sep = ""))
  }

  if (plotFA){
    plot.new()
    legend("center", legend = as.character(jColors$ages),
           col = jColors$color, pch = pch, title = "Ages",
           ncol = ncol)
  }

  return(l)
}


tRCA <- function(Y, ages, scale = 1e-6, q = 5, covMat = c("exp", "brownian"), plotPCA = TRUE, plotFA = TRUE, pch = 19){
  #' @title Temporal Residual Component Analysis
  #' @description This function corrects for temporal autocorrelation in genotypic data sets using
  #' a maximum-likelihood solution of a latent factor mixed effect model.
  #'
  #' Run \code{RShowDoc("temporalDR", package = "temporalDR")} to see the vignette for more information.
  #'
  #' @param Y A data frame or matrix of size N samples x M loci.
  #' @param ages A numeric vector of length N containing the ages of the samples.
  #' @param scale Float, parameter to tune.
  #' @param q Integer, the number of selected eigenvectors of the covariance.
  #' @param covMat The covariance matrix. A character string between "exp" and "brownian". See
  #' the Details section for more information.
  #' @param plotPCA Boolean. If \code{TRUE} standard PCA is performed (on centered and
  #' scaled data) and plotted.
  #' @param plotFA Boolean. If \code{TRUE}, plots the corrected graph.
  #' @param pch Integer or vector of integers. Specify symbols to use when plotting points.
  #'
  #' @details
  #' \strong{Model:}
  #'
  #' "tRCA" is based on \href{https://arxiv.org/ftp/arxiv/papers/1206/1206.4560.pdf}{Kalaitzis and Lawrence (2012)}.
  #' \code{tRCA} uses a latent factor mixed model:
  #' \deqn{Y = UV^T + ZW^T +E}
  #' where \eqn{U} and \eqn{V} are respectively the latent effects scores and loadings matrices, \eqn{Z} and \eqn{W} are respectively
  #' the fixed effects scores and loadings matrices, and \eqn{E} id the matrix of unknown noise variables.
  #' \code{tRCA} only returns the maximum-likelihood estimator of \eqn{U}.
  #'
  #' \strong{Covariance matrix:}
  #'
  #' \itemize{
  #'     \item If \code{covMat = "exp"}, \code{tRCA} uses a covariance matrix \code{Sigma = exp(-d(A_i, A_j)/theta)} with
  #'         \code{d(A_i, A_j)} the euclidean temporal distance between sample of age \code{A_i} and sample
  #'         of age \code{A_j}. The parameter \code{theta} is defined as \code{theta = scale * mean(d(A_i, A_j))}
  #'         with \code{scale} a parameter to tune.
  #'     \item If \code{covMat = "brownian"}, \code{tRCA} uses a Brownian covariance matrix \code{Sigma = min(s,t)}
  #'         with \code{s = Tmax - A_i, t = Tmax - A_j} and \code{Tmax = max(ages)}.
  #' }
  #' In either case, a small random perturbation is added to ages to avoid colinearity when computing the covariance matrix.
  #' Sometimes, the perturbation does not allow to get at least 2 latent factors. The \code{tRCA} function tries several
  #' values of \code{\link[base]{.Random.seed}} if necessary, and return the .Random.seed for which at least 2 latent factors
  #' have been found. Using this seed allows to reproduce results.
  #'
  #' The exponential covariance matrix uses a centered data set to solve the model, while the brownian covariance
  #' uses a 'raw' data set (neither centered nor scaled).
  #'
  #' Exponential and Brownian covariance fit errors ('obj$covFitError') are not comparable because of the
  #' different order of magnitude of the custom covariance matrices.
  #'
  #' For more details, run \code{RShowDoc("temporalDR", package = "temporalDR")}.
  #'
  #' @return A list of components
  #'      \item{U}{The latent effects score matrix, of size N x K.}
  #'      \item{Sigma}{The covariance matrix, of size N x N.}
  #'      \item{theta}{Tuned parameter in the exponential \code{Sigma}. If \code{covMat = "brownian"},
  #'      this parameter is NULL.}
  #'      \item{K}{Number of latent factors.}
  #'      \item{realCov}{The real sample covariance matrix, of size N x N. Computed on the raw \code{Y}
  #'      if \code{covMat = "brownian"} or on the centered and scaled data set if \code{covMat = "exp"}.}
  #'      \item{covFitError}{The mean squared of the difference \eqn{realCov - (UU^T + Sigma)}.  This error will be high with \code{covMat == "brownian"}
  #'     since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}
  #'      \item{randomSeed}{Vector. The `.Random.seed` for which a positive definite covariance matrix as been computed.}
  #'
  #'      If \code{plotFA} is \code{TRUE}, the first two columns of \code{U} (equivalent to principal components) gotten from
  #'     the correction method are plotted. \cr
  #'      If \code{plotPCA} is \code{TRUE}, standard PCA is also plotted, for comparison.
  #'
  #' @examples
  #' ## Load the data:
  #' data("onepop")
  #' Y <- onepop[, -1]
  #' ages <- onepop$ages
  #'
  #' ## tRCA with exponential covariance matrix
  #' obj1 <- tRCA(Y = Y,
  #'              ages = ages,
  #'              scale = 4e-7,
  #'              q = 36,
  #'              covMat = "exp")
  #'
  #' ## tRCA with Brownian covariance matrix
  #' obj2 <- tRCA(Y = Y,
  #'              ages = ages,
  #'              q = 36,
  #'              covMat = "brownian")
  #'
  #' @references
  #' Kalaitzis AA,Lawrence ND. Residual Component Analysis: Generalising PCA for
  #' more flexible inference in linear-Gaussian models.
  #' \emph{Proceedings of the 29th International Conference on Machine Learning}, 2012, pg. 209–216
  #'
  #' @seealso
  #' \code{\link{temporal_DR}}, \code{\link{imputeNA}}, \code{\link{tFA}}, \code{\link{tLFMM}}
  #'
  #' @export

  covMat <- match.arg(covMat)
  if (q < 2) stop("Argument 'q' must be >= 2.")
  if (!identical(length(ages), nrow(Y)))
    stop("Length of 'ages' should be equal to the number of rows of 'Y'.")

  Y <- as.matrix(Y)
  if (length(ages) >= 30) ncol = 3
  else ncol = 2

  ## Center and scale the data -------
  data <- center_and_scale(Y, ages)

  ## Plots' colors -------------------
  dat <- data.frame(ages = ages, Y)
  jColors <- with(dat, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))

  ## Standard PCA --------------------
  if (plotPCA){
    standard_PCA(dat, jColors, pch, 10)
    if (!plotFA){
      plot.new()
      legend("center", legend = as.character(jColors$ages),
             col = jColors$color, pch = pch, title = "Ages",
             ncol = ncol)
    }
  }

  initialSeed <- .Random.seed
  ## Compute Sigma, the covariance matrix -------
  K <- 0
  minK <- 2
  maxit <- 500
  it <- 0
  ## 1/ Distances matrix and ZZ'
  while (K < minK && it < maxit){
    randomSeed <- .Random.seed
    if (covMat == "exp"){
      ## Real covariance matrix
      realCov <- stats::cov(t(data))
      ## Exponential covariance matrix
      expo <- expo_covar(ages, scale, perturbation = TRUE)
      theta <- expo$theta
      ZZt <- expo$Sigma
    } else if (covMat == "brownian"){
      ## Real covariance matrix
      realCov <- stats::cov(t(Y))
      ## Brownian covariance matrix
      theta <- NULL
      ZZt <- brownian_covar(ages, perturbation = TRUE)
    }

    ## 2/ Spectral decomposition of ZZt: select q eigenvectors (zq), define residue sigma^2 = the (q+1)^th eigenvalue
    res <- eigen(ZZt)
    s2 <- res$values[q + 1]
    zq <- res$vectors[, seq_len(q)] %*% diag(res$values[seq_len(q)]) %*% solve(res$vectors)[seq_len(q), ]
    ## 3/ Covariance matrix Sigma
    Sigma <- zq + diag(s2, dim(data)[1])

    ## Solve the GEP ----------------------------
    if (covMat == "exp"){
      YYt <- (1 / dim(data)[2]) * (data %*% t(data))
    } else {
      YYt <- (1 / dim(Y)[2]) * (Y %*% t(Y))
    }
    z <- geigen::geigen(YYt, Sigma, only.values = FALSE, symmetric = TRUE) ## geigen() does not return eigenvalues in decreasing order, we must order them:
    sortValues <- sort.int(z$values, index.return = TRUE, decreasing = TRUE)
    order_list <- sortValues$ix
    ordered_values <- sortValues$x
    D <- diag(ordered_values)
    S <- z$vectors[, order_list]
    I <- diag(dim(data)[1])
    K <- sum(ordered_values > 1) ## All the generalized eigenvalues greater than one are selected to compute the latent effect scores matrix

    it <- it + 1
    if (it == maxit) stop("Unable to get at least 2 latent factors. Please try again with a larger value of parameter 'q'.")
  }

  ## Compute the latent effect scores matrix -------
  U <- Sigma %*% S[, seq_len(K)] %*% (D[seq_len(K), seq_len(K)] - I[seq_len(K), seq_len(K)]) ^ (1 / 2)

  rownames(U) <- ages

  ## Compute the covariance matrices fit error ------
  C <- realCov - (U %*% t(U) + Sigma)
  covFitError <- mse(C)

  l <- list("U" = U,
            "Sigma" = Sigma,
            "theta" = theta,
            "K" = K,
            "covMat" = covMat,
            "realCov" = realCov,
            "covFitError" = covFitError,
            "randomSeed" = randomSeed)

  if (covMat == "exp" && plotFA){
    plot_FA(U, ages, jColors, pch,
            title = paste("tRCA - Exponential covariance matrix\ntheta = ",
                          signif(theta, digits = 4),
                          "\tscale = ", signif(scale, digits = 4),
                          "\tK = ", K, "\tq = ", q, sep = ""))
  } else if (covMat == "brownian" && plotFA){
    plot_FA(U, ages, jColors, pch,
            title = paste("tRCA - Brownian covariance matrix\nK = ",
                          K, "\tq = ", q, sep = ""))
  }

  if (plotFA){
    plot.new()
    legend("center", legend = as.character(jColors$ages),
           col = jColors$color, pch = pch, title = "Ages",
           ncol = ncol)
  }

  .Random.seed <- initialSeed
  return(l)
}
