
plot_FA <- function(U, ages, jColors, pch = 19, title = NULL){
  #' @title Plot the corrected factors
  #'
  #' @param U The matrix of corrected scores.
  #' @param ages The vector of samples' ages.
  #' @param jColors A data frame holding the color scheme.
  #' @param title A string of characters. Title of the plot
  #'
  #' @details \code{jColors} must be created like this:
  #'
  #' \code{data("onepop")} \cr
  #' \code{jColors <- with(onepop, data.frame(ages = levels(as.factor(ages)),
  #'                                   color = I(rainbow(length(unique(ages))))))}
  
  layout(matrix(c(1, 1, 2, 3), 2, 2, byrow = TRUE))
  plot(U, col = jColors$color[match(rownames(U), jColors$ages)], pch = pch, main = title,
       xlab = "Factor 1", ylab = "Factor 2", cex=1.2)
  for (i in seq_len(2)) plot(ages, U[, i], pch = pch, main = paste("Factor ", i, sep = ""),
                             col = jColors$color[match(rownames(U), jColors$ages)],
                             ylab = paste("Factor ", i, sep=""), cex = 1.2)
  par(mfrow = c(1, 1))
}



plot.TemporalDR <- function(object, legend = TRUE, pch = 19, jColors = NULL, ...) {
  #' Plotting function for TemporalDR object
  #'
  #' Description.
  #' 
  #' @param object TemporalDR object 
  #' @param legend boolean, plot legend?
  #' @param pch plotting 'character', for plot and legend
  #' @param jColors data.frame with 'ages' and 'color' colums, specifying one color for each time sample
  #' @param ... Other options passed to plot().
  #' @return Nothing. Side-effect: plots graphs.
  #' 
  #' @examples 
  #' 
  #' ## Load the data
  #' data("onepop")
  #' Y <- onepop[, -1]
  #' ages <- onepop[, 1]
  #'
  #' ## Create object of classes "TemporalDR" and "tFA_exp"
  #' obj <- temporal_DR(Y, ages, naHandling = "mean", correctionMethod = "tFA",
  #'                     covMat = "exp", scale = 2e-8)
  #'     
  #' ## Plot the tFA results again but with a different color palette (e.g. terrain.colors)
  #' jColors <- data.frame(ages = levels(as.factor(obj$args$ages)),
  #'                         color = I(terrain.colors(length(unique(obj$args$ages)))))        
  #' plot(obj, jColors = jColors)
  #'
  #' 
  #' @export
  #' 
 
  if (is.null(jColors)) jColors <- data.frame(ages = levels(as.factor(object$args$ages)),
                                  color = I(rainbow(length(unique(object$args$ages)))))
  ncol <- ifelse(length(object$args$ages) >= 30,3,2)
  
  covString <- ifelse(object$args$covMat=="exp", "Exponential covariance matrix", "Brownian covariance matrix")
  print(covString)
  titleString = paste(object$args$correctionMethod, " - ", covString, "\n")
  for (param in c("theta", "scale", "K", "q", "lambda")) {
    if (param %in% names(object$args)) {
      titleString <- paste0(titleString, paste("\t", param, "=", signif(object$args[[param]],digits = 4)))
    }
  }
  
  plot_FA(object$U, object$args$ages, jColors, pch=pch, title = titleString)
  plot.new() 
  if (legend) legend("center", legend = as.character(jColors$ages),
                     col = jColors$color, pch = pch, title = "Ages", ncol = ncol)
}


