## ----setup, include = FALSE----------------------------------------------
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)

## ----echo=FALSE----------------------------------------------------------
library(temporalDR)
data("onepop")
Y <- onepop[, -1]
ages <- onepop[, 1]

Y <- as.matrix(Y)
rownames(Y) <- ages

jColors <- with(onepop, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))

pc <- prcomp(Y)

par(mar=c(5.1, 4.1, 4.1, 8.1), xpd = TRUE)
plot(pc$x, pch = 19, col = jColors$color[match(rownames(pc$x), jColors$ages)],
      main = "Temporal PCA")
legend("topright", legend = as.character(jColors$ages), 
       col = jColors$color, pch=19, cex=.6, title = "Ages (in generations)",
       ncol = 3, inset = c(-0.3,0))



## ------------------------------------------------------------------------
library(temporalDR)

## ------------------------------------------------------------------------
## Simulated genotypes for time differentiation in one population
data("onepop")

## ------------------------------------------------------------------------
## Complete data set of simulated genotypes
data("admixture_scenario")

## Incomplete data set of simulated genotypes
data("admixture_scenario_NA")

## ------------------------------------------------------------------------
data("onepop")
sum(is.na(onepop))

## ------------------------------------------------------------------------
colnames(onepop)[1]
onepop$ages

## ------------------------------------------------------------------------
# A vector of ages
ages <- onepop$ages

# The genotypes
Y <- onepop[, -1]

## ------------------------------------------------------------------------
scales <- seq(9e-9, 4e-8, length = 20)
errs_e <- errors_tFA(Y = Y, 
                     ages = ages, 
                     naHandling = "mean", 
                     K = 3, 
                     scales = scales, 
                     covMat = "exp", 
                     showPlot = TRUE,
                     showProgress = FALSE,
                     seed = 10)

## ------------------------------------------------------------------------
s <- errs_e$scales[which(errs_e$errors == min(errs_e$errors))]
set.seed(10)
obj_e <- temporal_DR(Y = Y, 
                     ages = ages, 
                     K = 3, 
                     scale = s, 
                     covMat = "exp", 
                     plotPCA = TRUE, 
                     plotFA = TRUE)
# We could also use the 'tFA' function:
# obj <- tFA(Y, ages, K = 3, scale = s, covMat = "exp", plotPCA = TRUE, plotFA = TRUE)

## ------------------------------------------------------------------------
K <- seq(5, 40)
errs_b <- errors_tFA(Y = Y, 
                     ages = ages, 
                     naHandling = "mean", 
                     K = K,
                     covMat = "brownian", 
                     showPlot = TRUE,
                     showProgress = FALSE,
                     seed = 10)

## ------------------------------------------------------------------------
obj_b <- temporal_DR(Y = Y, 
                     ages = ages, 
                     K = 17, 
                     correctionMethod = "tFA",
                     covMat = "brownian",
                     plotPCA = FALSE, 
                     plotFA = TRUE)

## ------------------------------------------------------------------------
data("admixture_scenario_NA")
sum(is.na(admixture_scenario_NA))

## ------------------------------------------------------------------------
# PCA of the original complete data set:
data("admixture_scenario")
Yc <- as.matrix(admixture_scenario[, -1])
rownames(Yc) <- admixture_scenario$ages

jColors <- with(admixture_scenario, data.frame(ages = levels(as.factor(ages)),
                                    color = I(rainbow(length(unique(ages))))))

pc <- prcomp(Yc)

par(mar=c(5.1, 4.1, 4.1, 8.1), xpd = TRUE)
plot(pc$x, col = jColors$color[match(rownames(pc$x), jColors$ages)],
     pch = 19, main = "Original data set")
legend("right", legend = as.character(jColors$ages), 
       col = jColors$color, pch=19, cex=.8, title = "Ages (in generations)",
       ncol = 3, inset = c(-0.32,0))

## ------------------------------------------------------------------------
# PCA of completed data set:
data("admixture_scenario_NA")
Y <- as.matrix(admixture_scenario_NA[, -1])
rownames(Y) <- admixture_scenario$ages

Y_mean <- imputeNA(Y, naHandling = "mean")
Y_ipca <- imputeNA(Y, naHandling = "IPCA")

pc_mean <- prcomp(Y_mean)
pc_ipca <- prcomp(Y_ipca)

plot(pc_mean$x, col = jColors$color[match(rownames(pc_mean$x), jColors$ages)],
     pch = 19, main = "Mean imputation")
plot(pc_ipca$x, col = jColors$color[match(rownames(pc_ipca$x), jColors$ages)],
     pch = 19, main = "IPCA")

## ------------------------------------------------------------------------
ages <- admixture_scenario_NA$ages
obj <- temporal_DR(Y = unname(Y), 
                   ages = ages,
                   naHandling = "mean", 
                   correctionMethod = "tLFMM",
                   covMat = "brownian",
                   K = 3, 
                   q = 2, 
                   lambda = 5,
                   plotPCA = FALSE)

## ------------------------------------------------------------------------
# Load the data
data("onepop")
Y <- onepop[, -1]
ages <- onepop$ages

# Create object of classes "TemporalDR" and "tFA_exp"
obj <- temporal_DR(Y = Y, 
                   ages = ages, 
                   naHandling = "mean", 
                   correctionMethod = "tFA",
                   covMat = "exp", 
                   scale = 2e-8, 
                   plotPCA = FALSE, 
                   plotFA = FALSE)

## ------------------------------------------------------------------------
# Score prediction on new data set
newdat <- onepop[5:25,]
scores <- temporalDR_predict(obj, newdat, numScores = 2)

# Plot
rownames(obj$U) <- ages
jColors <- with(onepop, data.frame(ages = levels(as.factor(ages)),
                                   color = I(rainbow(length(unique(ages))))))

par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
plot(obj$U, pch = 23, col = jColors$color[match(rownames(obj$U), jColors$ages)],
     xlab = "Factor 1", ylab = "Factor 2", main = "Samples projection")

legend("topright", legend = as.character(jColors$ages),
       col = jColors$color, pch=19, bty = 'n', cex=.6, title = "Ages (in generations)",
       ncol = 3, inset = c(-0.33,0))
legend("bottomright", legend = c("initial data set", "projection"), pch = c(23, 19),
       cex = .7, inset = c(-0.23, 0.1), bty = 'n')

rownames(scores) <- newdat[, 1]
points(scores, pch = 19, col = jColors$color[match(rownames(scores), jColors$ages)])

## ------------------------------------------------------------------------
# Delete random values
Y_NA <- as.matrix(newdat[, -1])
dd <- dim(Y_NA)[1] * dim(Y_NA)[2]
Y_NA[sample(dd, 0.2 * dd)] <- NA
newdatNA <- data.frame(ages = newdat$ages, Y_NA)

# Score prediction
scoresNA <- temporalDR_predict(obj, newdatNA, numScores = 2)

# Plot
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
plot(obj$U, pch = 23, col = jColors$color[match(rownames(obj$U), jColors$ages)],
     xlab = "Factor 1", ylab = "Factor 2", main = "Samples projection with NA")

legend("topright", legend = as.character(jColors$ages),
       col = jColors$color, pch=19, bty = 'n', cex=.6, title = "Ages (in generations)",
       ncol = 3, inset = c(-0.33,0))
legend("bottomright", legend = c("initial data set", "projection"), pch = c(23, 19),
       cex = .7, inset = c(-0.23, 0.1), bty = 'n')

rownames(scoresNA) <- newdatNA[, 1]
points(scoresNA, pch = 19, col = jColors$color[match(rownames(scoresNA), jColors$ages)])

## ----echo=FALSE, fig.height=6, fig.width=6-------------------------------
data("onepop")
Y <- onepop[, -1]
ages <- onepop[, 1]

Y <- as.matrix(Y)
rownames(Y) <- ages

N <- dim(Y)[1]
Y <- Y - matrix( rep( apply(Y,MARGIN=2,mean), N), nrow = N, byrow = T  )

covMat <- cov(t(Y))
require(gplots)
par(cex.main=.8)
heatmap.2(covMat, Colv=F, Rowv=F, trace='none', dendrogram='none', main="Covariance matrix of centered data set")

## ----echo=FALSE----------------------------------------------------------
diag(covMat) <- NA
C <- c(covMat)
dists <- as.matrix(dist(ages))
diag(dists) <- NA
D <- c(dists)

mod <- nls(C ~ a * exp(b * D) - c, start = c(a=0.04, b=2e-5, c=0.4))
a <- summary(mod)$coef[1, 1]
b <- summary(mod)$coef[2, 1]
c <- summary(mod)$coef[3, 1]

d <- unique(D)[-1]

par(mar=c(4.1, 4.1, 4.1, 10), xpd = TRUE)
plot(D, C, pch = 16, col = "darkgray", ylab = "Covariance",
     xlab = "Temporal distance between samples (in generations)")
lines(d, a * exp(b * d) - c, col = "darkcyan", lwd = 2)
legend("right", legend = c("Covariance values", "Exponential regression"),
       col = c("darkgray", "darkcyan"), pch = c(16, NA), lty = c(NA, 1), 
       lwd = c(NA, 2), cex = .8, inset = c(-0.42, 0), bty = 'n')

## ----echo=FALSE, fig.width=6, fig.height=6-------------------------------
rawCovar <- function(age){
  Sigma <- matrix(0, nrow = length(age), ncol = length(age))
  Tmax = max(age)
  for (i in 1:length(age))  for (j in 1:length(age)) Sigma[i,j] <- 1 + min(Tmax-age[i], Tmax-age[j])
  colnames(Sigma) = rownames(Sigma) = age
  
  return(Sigma)
}

Y <- as.matrix(onepop[, -1])
rownames(Y) <- ages
realRawCov <- cov(t(Y))
brownianCov <- rawCovar(ages)

par(cex.main=.8)
heatmap.2(realRawCov, Colv=F, Rowv=F, trace='none', dendrogram='none', main="Covariance matrix of raw data set")
heatmap.2(brownianCov, Colv=F, Rowv=F, trace='none', dendrogram='none', main="Brownian covariance matrix")

## ------------------------------------------------------------------------
Y <- onepop[, -1]
ages <- onepop$ages

obj1 <- tLFMM(Y= Y,
              ages = ages,
              K = 3,
              lambda = 1e-5,
              scale = 1.5e-8,
              covMat = "exp", 
              plotPCA = FALSE)

obj2 <- tLFMM(Y= Y,
              ages = ages,
              K = 3,
              lambda = 1e-5,
              scale = 1.5e-8,
              covMat = "exp",
              plotPCA = FALSE)

all.equal(obj1, obj2)

## ------------------------------------------------------------------------
obj1 <- tFA(Y = Y,
            ages = ages,
            K = 3,
            scale = 2e-08,
            covMat = "exp",
            plotPCA = FALSE,
            plotFA = FALSE)

obj2 <- tFA(Y = Y,
            ages = ages,
            K = 3,
            scale = 2e-08,
            covMat = "exp",
            plotPCA = FALSE,
            plotFA = FALSE)

all.equal(obj1, obj2)

## ------------------------------------------------------------------------
set.seed(10)
obj1 <- tFA(Y = Y,
            ages = ages,
            K = 3,
            scale = 2e-08,
            covMat = "exp",
            plotPCA = FALSE,
            plotFA = FALSE)

set.seed(10)
obj2 <- tFA(Y = Y,
            ages = ages,
            K = 3,
            scale = 2e-08,
            covMat = "exp",
            plotPCA = FALSE,
            plotFA = FALSE)

all.equal(obj1, obj2)

## ------------------------------------------------------------------------
obj1 <- tFA(Y = Y,
            ages = ages,
            K = 3,
            scale = 2e-08,
            covMat = "exp",
            plotPCA = FALSE,
            plotFA = FALSE)

.Random.seed <- obj1$randomSeed
obj2 <- tFA(Y = Y,
            ages = ages,
            K = 3,
            scale = 2e-08,
            covMat = "exp",
            plotPCA = FALSE,
            plotFA = FALSE)

all.equal(obj1, obj2)

## ----echo=FALSE----------------------------------------------------------
rm(list=ls())

## ------------------------------------------------------------------------
data("onepop")
Y <- onepop[,-c(1)]
ages <- onepop[,1]

## ------------------------------------------------------------------------
# We can use either the wrapper function 'temporal_DR' or the 'tFA' function
# since there are no missing values
set.seed(10)
obj1 <- tFA(Y, ages, K = 3, scale = 1e-8, covMat = "exp", plotPCA = FALSE)

## ------------------------------------------------------------------------
set.seed(10)
obj2 <- tFA(Y, ages, K = 3, scale = 2e-08, covMat = "exp", plotPCA = FALSE)

## ------------------------------------------------------------------------
set.seed(10)
obj3 <- tFA(Y, ages, K = 3, scale = 4e-8, covMat = "exp", plotPCA = FALSE)

## ------------------------------------------------------------------------
## Exponential covariance: try several values of 'scale' for given 'seed'
## and 'K'
errs_e <- errors_tFA(Y = Y,
                     ages = ages,
                     naHandling = "mean",
                     K = 3,
                     scales = seq(9e-9, 4e-8, length = 20),
                     covMat = "exp",
                     seed = 10,
                     showPlot = TRUE,
                     showProgress = FALSE)

best_scale <- errs_e$scales[which(errs_e$errors == min(errs_e$errors))]
set.seed(10)
obj <- tFA(Y, ages, K = 3, scale = best_scale, covMat = "exp", plotPCA = FALSE)

## Brownian covariance: try several values of 'K' for a given 'seed'
errs_b <- errors_tFA(Y = Y,
                     ages = ages,
                     naHandling = "mean",
                     K = seq(3,15),
                     covMat = "brownian",
                     seed = 10,
                     showPlot = TRUE,
                     showProgress = FALSE)
set.seed(10)
obj <- tFA(Y, ages, K = 10, covMat = "brownian", plotPCA = FALSE)

## ------------------------------------------------------------------------
obj4 <- tLFMM(Y = Y, 
              ages = ages, 
              q = 10, 
              K = 3,
              lambda = 1, 
              covMat = "brownian", 
              plotPCA = FALSE)

## ------------------------------------------------------------------------
obj5 <- tLFMM(Y = Y,
              ages = ages,
              q = 10,
              K = 3, 
              lambda = 1e-1, 
              covMat = "brownian",
              plotPCA = FALSE)

## ------------------------------------------------------------------------
obj6 <- tLFMM(Y = Y, 
              ages = ages, 
              q = 10, 
              K = 3, 
              lambda = 1e-2, 
              covMat = "brownian", 
              plotPCA = FALSE)

## ------------------------------------------------------------------------
qs <- c(2, 3, 4, 5, 10, 15)
lambdas <- c(1e-5, 1e-2, 1, 2, 5)

errs_b <- errors_tLFMM(Y = Y,
                       ages = ages,
                       naHandling = "mean",
                       K = 3, qs = qs,
                       lambdas = lambdas,
                       covMat = "brownian",
                       showProgress = FALSE)
best <- errs_b[which(errs_b$err == min(errs_b$err)), ]

set.seed(10)
obj <- tLFMM(Y = Y, 
              ages = ages, 
              q = best$q, 
              K = 3, 
              lambda = best$lambda, 
              covMat = "brownian", 
              plotPCA = FALSE)

## ------------------------------------------------------------------------
obj7 <- tRCA(Y, ages, q = 36, covMat = "brownian", plotPCA = FALSE)

## ------------------------------------------------------------------------
set.seed(10)
obj8 <- tRCA(Y, ages, scale = 1e-8, q = 3, covMat = "exp", plotPCA = FALSE)
set.seed(10)
obj9 <- tRCA(Y, ages, scale = 5e-7, q = 36, covMat = "exp", plotPCA = FALSE)

