% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_utilities.R
\name{plot_FA}
\alias{plot_FA}
\title{Plot the corrected factors}
\usage{
plot_FA(U, ages, jColors, pch = 19, title = NULL)
}
\arguments{
\item{U}{The matrix of corrected scores.}

\item{ages}{The vector of samples' ages.}

\item{jColors}{A data frame holding the color scheme.}

\item{title}{A string of characters. Title of the plot}
}
\description{
Plot the corrected factors
}
\details{
\code{jColors} must be created like this:

\code{data("onepop")} \cr
\code{jColors <- with(onepop, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))}
}
