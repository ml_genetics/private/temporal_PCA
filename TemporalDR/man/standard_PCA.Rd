% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/helpers.R
\name{standard_PCA}
\alias{standard_PCA}
\title{Standard PCA}
\usage{
standard_PCA(data, colors = NULL, pch = 19, K = 3)
}
\arguments{
\item{data}{A matrix or data frame, with ages in the first column and
genotypes in other columns.}

\item{colors}{A data frame holding the color scheme.}
}
\description{
Compute and plot the PCA of the centered and scaled data set.
}
\examples{
data("onepop")
colors <- with(onepop, data.frame(ages = levels(as.factor(ages)),
                                  color = I(rainbow(length(unique(ages))))))

standard_PCA(onepop, colors)
}
