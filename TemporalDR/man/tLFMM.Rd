% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/correction_methods.R
\name{tLFMM}
\alias{tLFMM}
\title{Temporal LFMM}
\usage{
tLFMM(Y, ages, q = 5, K = 3, lambda = 1, scale = 1e-06,
  covMat = c("exp", "brownian"), plotPCA = TRUE, plotFA = TRUE,
  pch = 19)
}
\arguments{
\item{Y}{A data frame or matrix of size N samples x M loci.}

\item{ages}{A numeric vector of length N containing the ages of the samples.}

\item{q}{Integer, the number of selected eigenvectors of the Brownian covariance matrix.}

\item{K}{Integer, the number of latent factors.}

\item{lambda}{The ridge regularization parameter.}

\item{scale}{Float, parameter to tune in the exponential covariance matrix.}

\item{covMat}{The covariance matrix. A character string between "exp" and "brownian". See
the Details section for more information.}

\item{plotPCA}{Boolean. If \code{TRUE} standard PCA is performed (on centered and
scaled data) and plotted.}

\item{plotFA}{Boolean. If \code{TRUE}, plots the corrected graph.}

\item{pch}{Integer or vector of integers. Specify symbols to use when plotting points.}
}
\value{
A list of components
     \item{U}{The latent effects scores matrix, of size N x K.}
     \item{V}{The latent effects loadings matrix, of size M x K.}
     \item{Z}{The fixed effects scores matrix, of size N x q.}
     \item{W}{The fixed effects loadings matrix, of size M x q.}
     \item{E}{The residual matrix, of size N x M.}
     \item{Sigma}{The covariance matrix, of size N x N.}
     \item{error}{The mean squared error of matrix E. This error will be high with \code{covMat == "brownian"}
    since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}
     \item{theta}{Tuned parameter in the exponential \code{Sigma}. If \code{covMat = "brownian"},
     this parameter is NULL.}
     \item{realCov}{The real sample covariance matrix, of size N x N. Computed on the raw \code{Y}
     if \code{covMat = "brownian"} or on the centered and scaled data set if \code{covMat = "exp"}.}
     \item{covFitError}{The mean squared of the difference \eqn{realCov - (UU^T + Sigma)}. This error will be high with \code{covMat == "brownian"}
    since the custom covariance matrix Sigma is not of the same order of magnitude as the real covariance matrix.}

     If \code{plotFA} is \code{TRUE}, the first two columns of \code{U} (equivalent to principal components) gotten from
    the correction method are plotted. \cr
     If \code{plotPCA} is \code{TRUE}, standard PCA is also plotted, for comparison.
}
\description{
This function corrects for temporal autocorrelation in genotypic data sets
by computing regularized least squares estimates for latent factor mixed models using
a ridge penalty.

Run \code{RShowDoc("temporalDR", package = "temporalDR")} to see the vignette for more information.
}
\details{
\strong{Model:}

"tLFMM" uses the \code{lfmm::lfmm_ridge()} function (\href{https://www.biorxiv.org/content/biorxiv/early/2018/05/13/255893.full.pdf}{Caye and François, 2018})
with the latent factor mixed model:
\deqn{Y = UV^T + ZW^T +E}
where \eqn{U} and \eqn{V} are respectively the latent effects scores and loadings matrices, \eqn{Z} and \eqn{W} are respectively
the fixed effects scores and loadings matrices, and \eqn{E} id the matrix of unknown noise variables. \eqn{Z} is given to \code{tLFMM} which returns
\eqn{U}, \eqn{V} and \eqn{W}.

\strong{Covariance matrix:}

\itemize{
    \item If \code{covMat = "exp"}, \code{tLFMM} uses a covariance matrix \code{Sigma = exp(-d(A_i, A_j)/theta)} with
        \code{d(A_i, A_j)} the euclidean temporal distance between sample of age \code{A_i} and sample
        of age \code{A_j}. The parameter \code{theta} is defined as \code{theta = scale * mean(d(A_i, A_j))}
        with \code{scale} a parameter to tune.
    \item If \code{covMat = "brownian"}, \code{tLFMM} uses a Brownian covariance matrix \code{Sigma = min(s,t)}
        with \code{s = Tmax - A_i, t = Tmax - A_j} and \code{Tmax = max(ages)}.
}

The exponential covariance matrix uses a centered data set to solve the model, while the brownian covariance
uses a 'raw' data set (neither centered nor scaled).

Exponential and Brownian errors ('obj$covFitError' and 'obj$error') are not comparable because of the
different order of magnitude of the custom covariance matrices.

For more details, run \code{RShowDoc("temporalDR", package = "temporalDR")}.
}
\examples{
## Load the data:
data("onepop")
Y <- onepop[, -1]
ages <- onepop$ages

## tLFMM with exponential covariance matrix
obj1 <- tLFMM(Y= Y,
              ages = ages,
              K = 3,
              lambda = 1e-5,
              scale = 1.5e-8,
              covMat = "exp")

## tLFMM with Brownian covariance matrix
obj2 <- tLFMM(Y = Y,
              ages = ages,
              q = 5,
              K = 3,
              lambda = 1e-5,
              covMat = "brownian")

}
\references{
Caye K, Francois O. LFMM 2.0: Latent factor models for confounder
adjustment in genome and epigenome-wide association studies,
\emph{Biorxiv}, 2018, \url{https://doi.org/10.1101/255893}
}
\seealso{
\code{\link{temporal_DR}}, \code{\link{imputeNA}}, \code{\link{tFA}}, \code{\link{tRCA}}, \code{\link{errors_tLFMM}}
}
