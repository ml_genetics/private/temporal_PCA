#' @title Temporal Dimension Reduction
#'
#' @description A package for dimension reduction and temporal autocorrelation correction in
#' genotypic data sets with time-separated samples.
#'
#' Run \code{RShowDoc("temporalDR", package = "temporalDR")} to see the vignette for more information.
#'
#'
#' @section Functions:
#' \code{\link{imputeNA}}: impute missing values in data sets. Two methods: mean imputation and iterative PCA.
#'
#' \code{\link{tFA}}, \code{\link{tLFMM}}, \code{\link{tRCA}}: correct for temporal autocorrelation by explicitely including
#' temporal information in the analysis model.
#'
#' \code{\link{temporal_DR}}: wrapper function. The user can choose the NA handling method and the correction model.
#'
#' \code{\link{errors_tFA}}, \code{\link{errors_tLFMM}}: compute the mean squared error for several values of the parameters.
#'
#' \code{\link{temporalDR_CV}}: perform cross-validation for tFA and tLFMM.
#'
#' \code{\link{temporalDR_predict}}: score prediction for new samples with loadings computed on a first data set.
#' 
#' \code{\link{plot.TemporalDR}}: plotting function for TemporalDR objects.
#'
#'
#' @section Data sets:
#' \code{\link{onepop}}: ages and genotypes of 45 individuals sampled in time from one population.
#'
#' \code{\link{admixture_scenario}}: ages and genotypes of 18 samples from an admixture scenario.
#'
#' \code{\link{admixture_scenario_NA}}: the same scenario as above, but 20\% of missing values per sample.
#'
#' @import geigen
#' @import lfmm
#' @import missMDA
#'
#' @docType package
#' @name temporalDR-package

NULL
