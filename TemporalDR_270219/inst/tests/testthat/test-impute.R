context("Missing values imputation")

test_that("mean iputation", {
  ## With matrix -----
  mat <- matrix(1, ncol = 10, nrow = 10)
  matNA <- mat
  diag(matNA) <- NA
  imputed_mat <- meanImpute(matNA)

  expect_is(imputed_mat, "matrix")
  expect_equal(dim(imputed_mat), c(10, 10))
  expect_true(all.equal(imputed_mat, mat))

  ## With data frame
  df <- as.data.frame(mat)
  dfNA <- as.data.frame(matNA)
  imputed_df <- meanImpute(dfNA)

  expect_is(imputed_df, "data.frame")
  expect_equal(dim(imputed_df), c(10, 10))
  expect_true(all.equal(imputed_df, df))
})


test_that("IPCA", {
  ## With matrix -----
  mat <- matrix(1:100, ncol = 10, nrow = 10)
  matNA <- mat
  diag(matNA) <- NA
  imputed_mat <- IPCA(matNA)

  expect_is(imputed_mat, "matrix")
  expect_equal(dim(imputed_mat), c(10, 10))

  ## With data frame
  df <- as.data.frame(mat)
  dfNA <- as.data.frame(matNA)
  imputed_df <- IPCA(dfNA)

  expect_is(imputed_df, "matrix")
  expect_equal(dim(imputed_df), c(10, 10))
})


test_that("imputeNA function", {
  expect_error(imputeNA(),
               "You must provide a data farame or matrix to complete in the 'Y' argument.")

  mat <- matrix(1:100, ncol = 10, nrow = 10)
  matNA <- mat
  diag(matNA) <- NA

  no_NA_mean <- imputeNA(mat, "mean")
  expect_true(all.equal(no_NA_mean, mat))

  no_NA_ipca <- imputeNA(mat, "IPCA")
  expect_true(all.equal(no_NA_ipca, mat))

  NA_mean <- imputeNA(matNA, "mean")
  expect_equal(dim(NA_mean), c(10, 10))
  expect_true(all.equal(colMeans(NA_mean), colMeans(matNA, na.rm = TRUE)))

  NA_ipca <- imputeNA(matNA, "IPCA")
  expect_equal(dim(NA_ipca), c(10, 10))
})
