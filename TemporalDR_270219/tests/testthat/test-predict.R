context("Scores prediction for projection")

test_that("prediction error messages", {
  ## No scores available
  expect_error(temporalDR_predict(),
               "No scores are available: provide an 'object' of class inheriting from 'TemporalDR' (see ?temporal_DR)",
               fixed = TRUE)

  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- temporal_DR(Y = Y, ages = ages, naHandling = "mean", correctionMethod = "tFA",
                     covMat = "exp", scale = 2e-8, plotPCA = FALSE, plotFA = FALSE)

  ## 'newdata' not a data frame or matrix
  newdat <- c(1, 2, 3)
  expect_error(temporalDR_predict(obj, newdat),
               "'newdata' must be a matrix or data frame.",
               fixed = TRUE)

  ## Wrong number of scores to predict
  newdat2 <- as.matrix(Y[1, ])
  expect_error(temporalDR_predict(obj, newdat2, numScores = 10),
               "Argument 'numScores' must be less than or equal to dim(object$V)[2].",
               fixed = TRUE)
})


test_that("tFA function", {
  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- tFA(Y = Y, ages = ages, covMat = "exp", plotPCA = FALSE, plotFA = FALSE)
  expect_is(obj, "list")

  ## 'newdata' omitted -> obj$U returned
  res1 <- temporalDR_predict(obj)
  expect_is(res1, "matrix")
  expect_true(all.equal(res1, obj$U))

  ## scores and 'newdata' provided -> bad class for 'object' -> UseMethod error
  newdat <- c(1, Y[1, ])
  newdat <- as.matrix(newdat)
  expect_error(temporalDR_predict(obj, newdat))
})


test_that("tFA with temporal_DR function expo", {
  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- temporal_DR(Y, ages, naHandling = "mean", correctionMethod = "tFA",
                     covMat = "exp", scale = 2e-8, plotPCA = FALSE, plotFA = FALSE)
  expect_is(obj, c("TemporalDR", "tFA_exp"))

  df <- data.frame(ages, Y)

  ## 1 sample to project
  newdat <- df[1, ]
  res1 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res1, "matrix")
  expect_equal(dim(res1), c(1, 2))
  expect_true(all.equal(unname(res1), Y[1,] %*% obj$V[, 1:2]))

  ## Predict scores of the same samples
  newdat <- df
  res2 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res2, "matrix")
  expect_equal(dim(res2), c(10, 3))
  expect_true(all.equal(res2, obj$U[, 1:3]))

  ## 1 sample with NAs
  newdat <- df[1, ]
  newdat[5] <- NA
  res3 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res3, "matrix")
  expect_equal(dim(res3), c(1, 2))

  ## Same samples with NAs
  newdat <- df
  newdat[1, 10] = newdat[5, 42] = newdat[10, 95] <- NA
  res4 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res4, "matrix")
  expect_equal(dim(res4), c(10, 2))
})


test_that("tFA with temporal_DR function brownian", {
  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- temporal_DR(Y, ages, naHandling = "mean", correctionMethod = "tFA",
                     covMat = "brownian", plotPCA = FALSE, plotFA = FALSE)
  expect_is(obj, c("TemporalDR", "tFA_brownian"))

  df <- data.frame(ages, Y)

  ## 1 sample to project
  newdat <- df[1, ]
  res1 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res1, "matrix")
  expect_equal(dim(res1), c(1, 3))
  expect_true(all.equal(unname(res1), Y[1,] %*% obj$V[, 1:3]))

  ## Predict scores of the same samples
  newdat <- df
  res2 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res2, "matrix")
  expect_equal(dim(res2), c(10, 3))
  expect_true(all.equal(res2, obj$U[, 1:3]))

  ## 1 sample with NAs
  newdat <- df[1, ]
  newdat[5] <- NA
  res3 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res3, "matrix")
  expect_equal(dim(res3), c(1, 2))

  ## Same samples with NAs
  newdat <- df
  newdat[1, 10] = newdat[5, 42] = newdat[10, 95] <- NA
  res4 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res4, "matrix")
  expect_equal(dim(res4), c(10, 2))
})


test_that("tLFMM function", {
  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- tLFMM(Y, ages, q = 5, K = 3, lambda = 1e-5, covMat = "brownian",
               plotPCA = FALSE, plotFA = FALSE)
  expect_is(obj, "list")

  ## 'newdata' omitted -> obj$U returned
  res1 <- temporalDR_predict(obj, numScores = 5)
  expect_is(res1, "matrix")
  expect_true(all.equal(res1, obj$U))

  ## scores and 'newdata' provided -> bad class for 'object' -> UseMethod error
  newdat <- c(1, Y[1, ])
  newdat <- as.matrix(newdat)
  expect_error(predict(obj, newdat))
})


test_that("tLFMM with temporal_DR function expo", {
  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- temporal_DR(Y, ages, naHandling = "mean", correctionMethod = "tLFMM",
                     covMat = "exp", lambda = 1e-5, scale = 1.5e-8,
                     plotPCA = FALSE, plotFA = FALSE)
  expect_is(obj, c("TemporalDR", "tLFMM_exp"))

  df <- data.frame(ages, Y)

  ## 1 sample to project
  newdat <- df[1, ]
  res1 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res1, "matrix")
  expect_equal(dim(res1), c(1, 3))

  ## Predict scores of the same samples
  newdat <- df
  res2 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res2, "matrix")
  expect_equal(dim(res2), c(10, 3))
  expect_true(all.equal(res2, obj$U[, 1:3]))

  ## Samples with NAs
  newdat <- df
  newdat[1, 10] = newdat[5, 42] = newdat[10, 95] <- NA
  res3 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res3, "matrix")
  expect_equal(dim(res3), c(10, 2))
})


test_that("tLFMM with temporal_DR function brownian", {
  Y <- matrix(sample(0:2, 1000, replace = TRUE), nrow = 10, ncol = 100)
  ages <- seq(1, 10)

  obj <- temporal_DR(Y, ages, naHandling = "mean", correctionMethod = "tLFMM",
                     covMat = "brownian", lambda = 1e-5, q = 5,
                     plotPCA = FALSE, plotFA = FALSE)
  expect_is(obj, c("TemporalDR", "tLFMM_brownian"))

  df <- data.frame(ages, Y)

  ## 1 sample to project
  newdat <- df[1, ]
  res1 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res1, "matrix")
  expect_equal(dim(res1), c(1, 3))

  ## Predict scores of the same samples
  newdat <- df
  res2 <- temporalDR_predict(obj, newdat, numScores = 3)
  expect_is(res2, "matrix")
  expect_equal(dim(res2), c(10, 3))
  expect_true(all.equal((Y - obj$Z %*% t(obj$W)) %*% obj$V[, 1:3], unname(res2)))

  ## Samples with NAs
  newdat <- df
  newdat[1, 10] = newdat[5, 42] = newdat[10, 95] <- NA
  res3 <- temporalDR_predict(obj, newdat, numScores = 2)
  expect_is(res3, "matrix")
  expect_equal(dim(res3), c(10, 2))
})
